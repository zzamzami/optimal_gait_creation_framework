function traj = getSLIPTrajectoryWrapper()
   % Map the generalized coordinates:
    % Keep the index-structs in memory to speed up processing
    persistent contStateIndices
    if isempty(contStateIndices)
        [~, ~, contStateIndices] = ContStateDefinition();
    end
    x      = y_(contStateIndices.x);
    y     = y_(contStateIndices.y);
    phi    = y_(contStateIndices.phi);
    yF     = y_(contStateIndices.yF);
    phiH   = y_(contStateIndices.phiH);
    phiT   = y_(contStateIndices.phiT);
    
    % Map the system parameters:
    % Keep the index-structs in memory to speed up processing
    persistent systParamIndices
    if isempty(systParamIndices)
        [~, ~, systParamIndices] = SystParamDefinition();
    end
     g = p(systParamIndices.g);
     lxC = p(systParamIndices.lxC);
     lyC = p(systParamIndices.lyC);
     lxB = p(systParamIndices.lxB);
     lyB = p(systParamIndices.lyB);
     lT = p(systParamIndices.lT);
     mS = p(systParamIndices.mS);
     mF = p(systParamIndices.mF);
     mT = p(systParamIndices.mT);
     jS = p(systParamIndices.jS);
     jF = p(systParamIndices.jF);
     jT = p(systParamIndices.jT);
     kF = p(systParamIndices.kF);
     kF0 = p(systParamIndices.kF0);
     bF = p(systParamIndices.bF);
     
     % x,y,phi,yF,phiH, dx,dy,dphi,dyF,dphiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF
     traj = getSLIPTrajectory(x,y,phi,yF,phiH, dx,dy,dphi,dyF,dphiH, g, lxC,lyC, mS,mF,jS,jF,kF,kF0,bF,dt);
     
end