% *************************************************************************
%
%
% Msc THESIS STEVE HEIM 2014
%
% function [contStateVec, contStateNames, contStateIndices] = ContStateDefinition()
% function y = ContStateDefinition()
%
% This MATLAB function defines the continuous state vector 'y' for a
% prismatic monopod with tail in 2D.  Besides serving as initial configuration of 
% the model, this file provides a definition of the individual components
% of the continuous state vector and an index struct that allows name-based
% access to its values.
%
% NOTE: This function is relatively slow and should not be executed within
%       the simulation loop.
%
% Input:  - NONE
% Output: - The initial continuous states as the vector 'contStateVec' (or 'y')
%         - The corresponding state names in the cell array 'contStateNames' 
%         - The struct 'contStateIndices' that maps these names into indices  
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPMAP, JUMPSET, 
%            DISCSTATEDEFINITION, SYSTPARAMDEFINITION, EXCTSTATEDEFINITION,
%            EXCTPARAMDEFINITION, 
%            VEC2STRUCT, STRUCT2VEC. 
%
function [contStateVec, contStateNames, contStateIndices] = ContStateDefinition()
    
    % All units are normalized to gravity g, total mass m_S, and
    % uncompressed leg length kF0.
    
    [systParamVec, ~, systParamIndices] = SystParamDefinition();
    kF0 = systParamVec(systParamIndices.kF0) ;       

    contState.x                    = 0;       % [kF0] horizontal position of the main body CoG
    contState.dx                   = 3.0;       % [sqrt(g*l_0)] ... velocity thereof
    contState.y                    = kF0+0.2; % [l_0] vertical position of the main body CoG
    contState.dy                   = 0;       % [sqrt(g*l_0)] ... velocity thereof
    contState.phi                  = pi/4;       % [rad] angle of the main body wrt horizontal (pitch)
    contState.dphi                 = 0;       % [rad/sqrt(l_0/g)] ... angular velocity thereof
    contState.yF                   = 0;      % [kF0] initial leg extension
    contState.dyF                  = 0;       % [sqrt(g*l_0)] ... velocity thereof
    contState.phiH                 = 0;%-pi/4;       % [rad] hip rotation
    contState.dphiH                = 0;       % [rad/sqrt(l_0/g)] ... angular velocity thereof
    contState.phiT                 = 0;       % [rad] tail rotation
    contState.dphiT                = 0;       % [rad/sqrt(l_0/g)] ... angular velocity thereof
    contState.time                 = 0;       %  time that has passed since the start of the step
    contState.posWork              = 0;       %  positive mechanical work of the actuators
    contState.dampingLosses        = 0;       % mechanical damping losses
    contState.resistanceLosses     = 0;       % electrical damping losses (motor resistances)
    
    [contStateVec, contStateNames] = Struct2Vec(contState);
    contStateIndices = Vec2Struct(1:1:length(contStateVec),contStateNames);
end
